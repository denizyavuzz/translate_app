function Translate(word,language){
    this.apikey = "trnsl.1.1.20190627T164601Z.3248137d0290e99a.95e8de8c3285eead9ca0dfdc758acb4a60c9414d";
    this.word = word;
    this.language = language;

    this.xhr = new XMLHttpRequest();
};

Translate.prototype.translateWord = function(callback){
   
    const endpoint = `https://translate.yandex.net/api/v1.5/tr.json/translate?key=${this.apikey}&text=${this.word}&lang=${this.language}`;


    this.xhr.open("GET", endpoint);

    this.xhr.onload = () =>{
        if(this.xhr.status === 200){
            const json = JSON.parse(this.xhr.responseText);

            const text = json.text[0];

            callback(null,text);
           
        }
        else{
            callback("Bir hata oluştu", null);
        }
    }

    this.xhr.send();
};

Translate.prototype.changeParameters = function(newWord, newLanguage){
    this.word = newWord;
    this.language = newLanguage;
}